.. scenario documentation master file, created by
   sphinx-quickstart on Wed Oct 26 10:10:15 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to scenario's documentation!
====================================

.. toctree::
   :maxdepth: 5

   1
   res-1-a
   res-1-b
   res-1-c-b
   res-1-c
   2




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
